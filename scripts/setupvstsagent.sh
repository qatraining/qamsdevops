#!/bin/bash

#update and pre-reqs
sudo apt-get update
sudo apt-get install -y openjdk-8-jdk libunwind8 libcurl3

#make a directory for the agent
mkdir /vstsagent
chown $1:$1 /vstsagent
cd /vstsagent

#get the file
wget https://github.com/Microsoft/vsts-agent/releases/download/v2.107.0/vsts-agent-ubuntu.16.04-x64-2.107.0.tar.gz

#unzip and run
sudo -u $1 tar xvf vsts-agent-ubuntu.16.04-x64-2.107.0.tar.gz
sudo -u $1 bash ./config.sh --unattended --url $2 --auth PAT --token $3 --pool default --agent $4
